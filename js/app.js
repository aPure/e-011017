if(navigator.userAgent.match(/Android/i)){
  document.head.insertAdjacentHTML( 'beforeEnd', '<link rel="stylesheet" href="css/framework7.material.min.css">');
  document.head.insertAdjacentHTML( 'beforeEnd', '<link rel="stylesheet" href="css/framework7.material.colors.min.css">');
}else if(navigator.userAgent.match(/iPhone|iPad|iPod/i)){
  document.head.insertAdjacentHTML( 'beforeEnd', '<link rel="stylesheet" href="css/framework7.ios.min.css">');
  document.head.insertAdjacentHTML( 'beforeEnd', '<link rel="stylesheet" href="css/framework7.ios.colors.min.css">');
}else{
  document.body.innerHTML = "本活動只能使用手機參加，請掃描以下QRcode參加活動。 <br /><br /> <div> <img src='img/qrcode.png' style='display:block; margin: 0 auto;'/></div>";
  document.body.style.textAlign = "center";
  document.body.style.fontSize = "200%";
}

var myApp = new Framework7();
var $$ = Dom7;

var mainView = myApp.addView('.view-main', {
  // Because we want to use dynamic navbar, we need to enable it for this view:
  //dynamicNavbar: true
  domCache: true
});

// if(!storeDatas || storeDatas.store_name == ""){
//   mainView.router.load({pageName: 'index'});
// }
// else{
  //console.log(storeDatas.store_name + match_store_number(storeDatas.store_name));
  $$('.qr_code').show();
  $$('.next_button').show();
  $$('.bar_code').show();
  $$('.good_luck_button').show();
  // $$('.qr_code').attr('src', 'img/qrcode/'+match_store_number(storeDatas.store_name)+'.jpg');
// }
// $$('.next_button').on('click',function(){
//   myApp.alert("請期待下次的活動", "本活動已於8/31結束");
// });

$$('.set_store').on('click', function(){
  var storeName = myApp.formToData('#store');
  if(storeName.store_name == ""){
    $$('.store_name_message p').text("門市必要");
    $$('.store_name_message p').css({color: 'red'});
  }else{
    var storedData = myApp.formStoreData('store', storeName);
    mainView.router.load({pageName: 'qrcode'});
    window.location.reload(false);
  }
});

$$('.form-to-data').on('click', function(){
  var formData = myApp.formToData('#identity');
  // If name or celphone is empty
  if(formData.name == "" || formData.cel == "" || formData.store_name == ""){
    $$('.error-message').show();
    $$('.error-message p').text("請輸入你的姓名與電話喔與店點");
  }else{
    //console.log(formData);
    //13.113.64.137
    $$('.line_link').attr('href', match_store_line_url(formData.store_name));
    myApp.alert("您將在5分鐘內，收到一封領取通知簡訊，敬請保留並於10/1到店點現場出示使用", "", function(){

      var data = {name: formData.name, cel: formData.cel, store: formData.store_name};
      var api_url = 'http://13.113.64.137/apure_api/index.php?action/event/';
      $$.get(api_url, data, function(data, status, xhr){
        var resp = JSON.parse(xhr.response);
        if(resp.check == 1){
          myApp.alert("您已參與過體驗活動，如果您喜歡我們的產品，歡迎到aPure官方網站參觀選購。", "對不起");
        }else if(resp.check == 2){
          myApp.alert("你晚了一步，這間店點名額已滿，您可以考慮選擇其他店點領取，或是參加"+match_store_event_name(formData.store_name)+"，立即加入LINE我們將主動通知您活動訊息。", "很抱歉");
        }else{
          mainView.router.load({pageName: 'thanks'});
        }
      });
    });
    

  }
});

// $$('.good_luck_button').on('click', function(){
//   var deletion = myApp.formDeleteData('phone_validate');
//   myApp.confirm($$('#user_code').text(), "不要忘記抄下客人的領取序號", function(){
//     window.location.reload(false);
//   });
// });

function match_store_event_name(store){
  //console.log("akd");
  switch(store){
    case '新光信義A11–5F':
      return '10/19來新光信義A11周年慶';
      break;
    case '台中新光三越-12F':
      return '10/5來台中新光周年慶';
      break;
    case '桃園遠百-8F':
      return '10/26來桃園遠百周年慶';
      break;
    case '高雄大遠百-9F':
      return '10/26來高雄大遠百周年慶';
      break;
    case '高雄漢神巨蛋-8F':
      return '10/12來高雄漢神巨蛋周年慶';
      break;
    case '新光南西-8F':
      return '10/5來新光南西周年慶';
      break;
    case '新光信義A8–B1':
      return '10/19來新光信義A8周年慶';
      break;
  }
}


function match_store_line_url(store){
  //console.log("akd");
  switch(store){
    case '新光信義A11–5F':
      return 'https://line.me/R/ti/p/%40qcw2755s';
      break;
    case '台中新光三越-12F':
      return 'https://line.me/R/ti/p/%40exe1483f';
      break;
    case '桃園遠百-8F':
      return 'https://line.me/R/ti/p/%40sya5680k';
      break;
    case '高雄大遠百-9F':
      return 'https://line.me/R/ti/p/%40pyj0386z';
      break;
    case '高雄漢神巨蛋-8F':
      return 'https://line.me/R/ti/p/%40pau8914v';
      break;
    case '新光南西-8F':
      return 'https://line.me/R/ti/p/%40lsz4228d';
      break;
    case '新光信義A8–B1':
      return 'https://line.me/R/ti/p/%40cmf1588w';
      break;
  }
}